package net.spottog.i18n.global;

import java.util.Date;
import java.util.Locale;
/**
 * Ein Textobjekt.
 *
 */
public class TextWithLanguageAnswer implements ITextWithLanguage {
	private Date expire;
	private String text;
	private String searchString;
	private Locale language;
	/**
	 * Setzt den Zeitpunkt, zu welchem das element ungültig wird.
	 * @param expire der Zeitpunkt
	 */
	public void setExpire(Date expire) {
		this.expire = expire;
	}
	/**
	 * Setzt den übersetzten Text, des elementes.
	 * @param text der Text
	 */
	public void setText(String text) {
		this.text = text;
	}
	/**
	 * Setzt den Suchstring unter welchem das element in einem Save gefunden werden kann.
	 * @param searchString zu suchenende Wort/Satz
	 */
	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}
	/**
	 * Setzt die Sprache in welcher der Text gesucht wird oder Vorliegt.
	 * @param language Die Sprache.
	 */
	public void setLanguage(Locale language) {
		this.language = language;
	}

	@Override
	public Date getExpire() {
		return expire;
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public String getSearchString() {
		return searchString;
	}

	@Override
	public Locale getLanguage() {
		return language;
	}

	@Override
	public boolean isValid() {
		return true;
	}

}
