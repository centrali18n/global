package net.spottog.i18n.global;

import java.util.Date;
/**
 * Ein Textelement.
 * Diese Schnitstelle ist vor allem vorgesehen um über das Netz übertragen zu werden, da nur ein ablaufdatum übertragen wird und keine gültigkeitsprüfung.
 *
 */
public interface ITextWithLanguageAnswer extends TextWithLanguageBase {
	/**
	 * Wann läuft das Objekt ab.
	 * @return das Datum.
	 */
	Date getExpire();
	/**
	 * Der Text zu einem Suchstring in einer Sprache.
	 * @return der Text.
	 */
	String getText();

}
