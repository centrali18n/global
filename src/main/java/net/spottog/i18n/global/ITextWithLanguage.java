package net.spottog.i18n.global;

/**
 * Das Standrd objekt welches einen Text in einer Sprache Representiert.
 * Ein Text Eintrag besitzt eine gültigkeit und kann ablaufen.
 *
 */
public interface ITextWithLanguage extends ITextWithLanguageAnswer {
	/**
	 * Gibt an ob ein Text noch gültig ist, andersfalls ist er abgelaufen.
	 * @return true wenn der Text noch gültig ist.
	 */
	boolean isValid();
}
