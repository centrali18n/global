package net.spottog.i18n.global;

import java.util.Locale;
/**
 * Ein Suchstring in einer Sprache
 */
public interface TextWithLanguageBase {
	/**
	 * ein Wort ein Satz oder ein Teilsatz.
	 * @return der zugehörige Such Text.
	 */
	String getSearchString();
	/**
	 * Die Sprache in welcher der Text gesucht wird oder Vorliegt.
	 * @return die Sprache.
	 */
	Locale getLanguage();
}
