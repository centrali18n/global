package net.spottog.i18n.global;

import java.util.Locale;

public class TextWithLanguageRequest implements TextWithLanguageBase {
	public TextWithLanguageRequest() {};
	public TextWithLanguageRequest(final String apiKey, final String searchString, final Locale language) {
		this.apiKey = apiKey;
		this.searchString = searchString;
		this.language = language;
	}
	/**
	 * APi Key an welchem der Benutzer Hängt.
	 * @return the key.
	 */
	public String getApiKey() 									{ return apiKey; }
	/**
	 * APi Key an welchem der Benutzer Hängt.
	 * @param apiKey the key.
	 */
	public void setApiKey(final String apiKey) 					{ this.apiKey = apiKey; }
	private String apiKey;
	
	
	@Override
	public String getSearchString() 							{ return searchString; }
	/**
	 * Setzt den Suchstring unter welchem das element in einem Save gefunden werden kann.
	 * @param searchString zu suchenende Wort/Satz
	 */
	public void setSearchString(final String searchString)		{ this.searchString = searchString; }
	private String searchString;
	
	
	@Override
	public Locale getLanguage() 								{ return language; }
	/**
	 * Setzt die Sprache in welcher der Text gesucht wird oder Vorliegt.
	 * @param Language Die Sprache.
	 */
	public void setLanguage(final Locale Language)				{ this.language = Language; }
	private Locale language;
}
