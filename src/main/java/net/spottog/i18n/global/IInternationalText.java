package net.spottog.i18n.global;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
/**
 * Ein Verkettetes Textelement.
 *
 */
public abstract class IInternationalText implements ITextWithLanguage{
	/**
	 * Der TextSave, bzw. der Beginn der Verkettung von Verschiedenen TextSave's welche befragt werden. 
	 */
    public static ITextSave Save;
    private ITextWithLanguage InternationaText;
    /**
     * Das Textelement in der Verkettung an der Aktuellen Position
     * @return das Text Element
     */
    public ITextWithLanguage getInternationaText() {
		return InternationaText;
	}
    /**
     * Das Textelement in der Verkettung an der Aktuellen Position
     * @param internationaText das Text Element
     */
	public void setInternationaText(ITextWithLanguage internationaText) {
		InternationaText = internationaText;
	}
	private IInternationalText Second;
	/**
	 * Reperesentiert das Nächste Element in der Verkettung.
	 * So kann dies wie eine Liste durchlaufen werden.
	 * @return das Element.
	 */
    public IInternationalText getSecond() {
		return Second;
	}
    /**
     * Reperesentiert das Nächste Element in der Verkettung.
	 * So kann dies wie eine Liste durchlaufen werden.
     * @param second das Element.
     */
	protected void setSecond(IInternationalText second) {
		Second = second;
	}
	private String SearchString;
    public String getSearchString() {
		return SearchString;
	}
	/**
	 * Setzt den Suchstring unter welchem das element in einem Save gefunden werden kann.
	 * @param searchString zu suchenende Wort/Satz
	 */
	public void setSearchString(String searchString) {
		SearchString = searchString;
	}
	private String TwoLetterISOLanguageName;
	/**
	 * Die Sprache in welcher der Text gesucht wird oder Vorliegt.
	 * @return Jede Sprache hat einen ISO Standardisierten code z.b. de für deutsch bzw. en für englisch, dieser code wird hier zurückgegeben
	 */
    public String getTwoLetterISOLanguageName() {
		return TwoLetterISOLanguageName;
	}
	/**
	 * Setzt die Sprache in welcher der Text gesucht wird oder Vorliegt.
	 * @param twoLetterISOLanguageName Jede Sprache hat einen ISO Standardisierten code z.b. de für deutsch bzw. en für englisch, dieser code wird hier erwartet.
	 */
	public void setTwoLetterISOLanguageName(String twoLetterISOLanguageName) {
		TwoLetterISOLanguageName = twoLetterISOLanguageName;
	}
	//public CultureInfo Language { get => CultureInfo.GetCultureInfoByIetfLanguageTag(TwoLetterISOLanguageName); set => TwoLetterISOLanguageName = value.TwoLetterISOLanguageName; }
    public Locale getLanguage() {
    	return Locale.forLanguageTag(TwoLetterISOLanguageName);
    }
	/**
	 * Setzt die Sprache in welcher der Text gesucht wird oder Vorliegt.
	 * @param locale Die Sprache.
	 */
    public void setLanguage(Locale locale) {
    	TwoLetterISOLanguageName = locale.getLanguage();
    }
	private String Text;
    public String getText() {
		return Text;
	}
	/**
	 * Setzt den übersetzten Text, des elementes.
	 * @param text der Text
	 */
	public void setText(String text) {
		Text = text;
	}
	private Date Expire;
    public Date getExpire() {
		return Expire;
	}
	/**
	 * Setzt den Zeitpunkt, zu welchem das element ungültig wird.
	 * @param expire der Zeitpunkt.
	 */
	public void setExpire(Date expire) {
		Expire = expire;
	}
	public boolean isValid(){
		return Calendar.getInstance().getTimeInMillis() >  Expire.getTime();
	}
	/**
	 * Hilfsmethode, welche die derzeitige Standardsprache zurückgibt
	 * @return Die Eingestellte Sprache.
	 */
	public static Locale DefaultCultureInfo() {
		return Locale.getDefault();
	}
	/**
	 * Gibt den Text in der Ausgewählten Sprache zurück.
	 * @param language die Sprache
	 * @return der Text
	 */
    public abstract String toString(Locale language);
    /**
     * Gibt den Text in der Standard Sprache zurück.
     * @see DefaultCultureInfo
     * @return der Text
     */
    @Override
    public String toString(){
        return toString(DefaultCultureInfo());
    }

}
