package net.spottog.i18n.global;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
/**
 * Implementation, eines Text Elementes, welches einen Text, ein Suchstring und eine Sprache hat.
 * Dieses Objekt kann ablaufen.
 */
public class TextWithLanguage implements ITextWithLanguage {
	private Date Expire;
	private String SearchString;
	private String Text;
	private Locale Language;
	/**
	 * Implementation, eines Text Elementes, welches einen Text, ein Suchstring und eine Sprache hat.
	 * Dieses Objekt kann ablaufen.
	 * @param anser als Quelle dient ein ITextWithLanguageAnswer objekt, welches weniger Funktionalität besitzt, dafür schneller übertragen werden kann.
	 */
	public TextWithLanguage(ITextWithLanguageAnswer anser) {
		this(anser.getSearchString(), anser.getText(), anser.getLanguage(), anser.getExpire());
	}
	/**
	 * Implementation, eines Text Elementes, welches einen Text, ein Suchstring und eine Sprache hat.
	 * Dieses Objekt kann ablaufen.
	 * @param searchstring das Suchwort.
	 * @param text der Übersetzte Text.
	 * @param twoLetterISOLanguageName code für die Sprache.
	 */
	public TextWithLanguage(String searchstring, String text, String twoLetterISOLanguageName) {
		this(searchstring, text,  Locale.forLanguageTag(twoLetterISOLanguageName), null);
	}
	/**
	 * Implementation, eines Text Elementes, welches einen Text, ein Suchstring und eine Sprache hat.
	 * Dieses Objekt kann ablaufen.
	 * @param searchstring das Suchwort.
	 * @param text der Übersetzte Text.
	 * @param language die Sprache.
	 */
    public TextWithLanguage(String searchstring, String text, Locale language) {
    	this(searchstring, text,  language, null);
	}
    /**
	 * Implementation, eines Text Elementes, welches einen Text, ein Suchstring und eine Sprache hat.
	 * Dieses Objekt kann ablaufen.
	 * @param searchstring das Suchwort.
	 * @param text der Übersetzte Text.
	 * @param language die Sprache.
     * @param expire Zeitpunkt, zu welchem das objekt abläuft.
     */
	public TextWithLanguage(String searchstring, String text, Locale language, Date expire)
    {
		if(null == expire) {
			Calendar c = Calendar.getInstance();
			c.add(1, Calendar.MINUTE);
			expire = c.getTime();
		}
        setSearchString(searchstring);
        setText(text);
        setLanguage(language);
        setExpire(expire);
    }

	
	public Locale getLanguage() {
    	return Language;
    }
	/**
	 * Setzt die Sprache in welcher der Text gesucht wird oder Vorliegt.
	 * @param locale Die Sprache.
	 */
    public void setLanguage(Locale locale) {
    	Language = locale;
    }
	/**
	 * Die Sprache in welcher der Text gesucht wird oder Vorliegt.
	 * @return Jede Sprache hat einen ISO Standardisierten code z.b. de für deutsch bzw. en für englisch, dieser code wird hier zurückgegeben
	 */
	public String getTwoLetterISOLanguageName() {
		return Language.getLanguage();
	}
	/**
	 * Setzt die Sprache in welcher der Text gesucht wird oder Vorliegt.
	 * @param twoLetterISOLanguageName Jede Sprache hat einen ISO Standardisierten code z.b. de für deutsch bzw. en für englisch, dieser code wird hier erwartet.
	 */
	public void setTwoLetterISOLanguageName(String twoLetterISOLanguageName) {
		Language = Locale.forLanguageTag(twoLetterISOLanguageName);
	}
	public Date getExpire() {
		return Expire;
	}
	public String getSearchString() {
		return SearchString;
	}
	public String getText() {
		return Text;
	}
	public boolean isValid() {
		return Expire.after(new Date());
	}
	/**
	 * Setzt den Zeitpunkt, zu welchem das element ungültig wird.
	 * @param expire der Zeitpunkt
	 */
	public void setExpire(Date expire) {
		Expire = expire;
	}
	/**
	 * Setzt den Suchstring unter welchem das element in einem Save gefunden werden kann.
	 * @param searchString zu suchenende Wort/Satz
	 */
	public void setSearchString(String searchString) {
		SearchString = searchString;
	}
	/**
	 * Setzt den übersetzten Text, des elementes.
	 * @param text der Text
	 */
	public void setText(String text) {
		Text = text;
	}



}
