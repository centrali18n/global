package net.spottog.i18n.global;

import java.util.Locale;
/**
 * Das Interface welches Alle TextSpeicher implementieren.
 * Die TextSpeicher enthalten die eigentlichen Texte.
 * Wenn ein TextSpeicher einen Text nicht enthält befrägt er seinen nachfolger, dadurch wird eine verkettung aufgebaut.
 * Ich Spreche von einem interface, weil es für die Verwendung besser passt implementiert ist allerdings eine abstrakte klasse, da Java interfaces keinerlei implementierungen enthalten können ich die verkettung allerdings in der Basisklasse implementiert haben wollte.
 * @author Fabian Spottog
 *
 */
public abstract class ITextSave {
	private ITextSave Next;
	/**
	 * Instanziiert einen TextSpeicher.
	 * @param next  das nächste objekt für die verkettung
	 */
    public ITextSave(ITextSave next)
    {
        Next = next;
    }
    /**
     * Gibt das nächste objekt in der verkettung zurück.
     * @return ein weiterer TextSave
     */
	public ITextSave getNext() {
		return Next;
	}
	/**
	 * Es Wird ein Text in einer Bestimmten Sprache gesucht. @See GetLocal
	 * Wird der text nicht gefunden wird der nächste TextSave in der Verkettung gesucht.
	 * @param search der zu suchende Text
	 * @param Language die zu suchende Sprache
	 * @return das Text Objekt
	 */
    public ITextWithLanguage Get(String search, Locale Language)
    {
        ITextWithLanguage local = GetLocal(search, Language);
        if (null != local && (Next == null || local.isValid()))
            return local;
        if (null == Next)
            return null;
        ITextWithLanguage global =  Next.Get(search, Language);
        if (null != global)
        	Add(global);
        return global;
    }
    /**
     * Es wird Lokal gesucht.
     * @param search der zu suchende Text
     * @param Language die zu suchende Sprache
     * @return das Text Objekt bzw. null wenn nichts gefunden wird.
     */
    public abstract ITextWithLanguage GetLocal(String search, Locale Language);
    /**
     * Frägt ab, ob ein TextSpeicher ein Bestimmtes Element enthält
     * @param search das Such Wort, bzw. Satz.
     * @param Language die Sprache.
     * @return true, wenn ein wert enthalten ist.
     */
    public boolean Contains(String search, Locale Language) {
    	return null != GetLocal(search, Language);
    }
    /**
     * Die Standardimplementierung von Add
     * Wenn ein TextSpeicher Texte Hunzufügen kann, wird diese Methode überschrieben.
     * @param internationaText das Textobjekt welches hinzugefügt werden soll.
     */
    public void Add(ITextWithLanguage internationaText)
    {
    	if(internationaText == null) return;
    	if(!internationaText.isValid())
    		throw new IllegalArgumentException("new Value is not Valid");
    	if(null != Next)
    		Next.Add(internationaText);
    }
}
