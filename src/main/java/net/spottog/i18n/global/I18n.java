package net.spottog.i18n.global;

import java.util.Locale;

/**
 * Standard Klasse für einen Text.
 */
public class I18n extends IInternationalText {
	// public static CultureInfo defaultCultureInfo = CultureInfo.CurrentCulture;
	
	/**
	 * Erstellt ein Standard Text Element
	 * @param text der Text
	 * @param second Ein Weiteres Text Element, als Verkettung
	 */
	public I18n(String text, I18n second) {
		this(text, second, null);
	}
	/**
	 * Erstellt ein Standard Text Element
	 * @param text der Text
	 */
	public I18n(String text) {
		this(text, null, null);
	}
	/**
	 * Erstellt ein Standard Text Element
	 * @param text der Text
	 * @param cultureInfo Die Sprache, in welche der Text gleich Gespeichert wird.
	 */
	public I18n(String text, Locale cultureInfo) {
		this(text, null, cultureInfo);
	}
	/**
	 * Erstellt ein Standard Text Element
	 * @param text der Text
	 * @param second Ein Weiteres Text Element, als Verkettung
	 * @param cultureInfo Die Sprache, in welche der Text gleich Gespeichert wird.
	 */
	public I18n(String text, I18n second, Locale cultureInfo) {
		setInternationaText(new TextWithLanguage(text, text, cultureInfo));
		setSecond(second);
		if (null != Save)
			Save.Add(getInternationaText());
	}
	/**
	 * Gibt den Text in der Übergebenen Sprache wieder.
	 */
	@Override
	public String toString(Locale language) {
		StringBuilder sb = new StringBuilder();
		sb.append(Save != null ? Save.Get(getInternationaText().getSearchString(), language).getText()
				: getInternationaText().getText());
		if (null != getSecond())
			sb.append(getSecond().toString(language));
		return sb.toString();
	}

}
