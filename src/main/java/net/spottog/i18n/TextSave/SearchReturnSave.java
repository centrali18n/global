package net.spottog.i18n.TextSave;

import java.util.Locale;

import net.spottog.i18n.global.ITextSave;
import net.spottog.i18n.global.ITextWithLanguage;
import net.spottog.i18n.global.TextWithLanguage;
/**
 * gibt den Suchstring zurück.
 */
public class SearchReturnSave extends ITextSave {
	private boolean Printout;
	/**
	 * Gibt an ob bei einer Suche nach dem Text, der Text auf der konsole ausgegeben werden soll,
	 * dies ist hilfreich zum debugen und zu sehen, ob es noch texte gibt, die eingtragen werden sollten.
	 * @return ob die texte ausgegeben werden.
	 */
	public boolean getPrintout() {
		return Printout;
	}
	/**
	 * Gibt an ob bei einer Suche nach dem Text, der Text auf der konsole ausgegeben werden soll,
	 * dies ist hilfreich zum debugen und zu sehen, ob es noch texte gibt, die eingtragen werden sollten.
	 * @param printout ob die texte ausgegeben werden.
	 */
	public void setPrintout(boolean printout) {
		Printout = printout;
	}
	/**
	 * inititialisiert eine neue instance ohne Printout.
	 * Kann allerdings nachträglich geändert werden.
	 * @param next muss null sein.
	 */
	public SearchReturnSave(ITextSave next) {
		this(next, false);
	}
	/**
	 * inititialisiert eine neue instance
	 * @param next next muss null sein.
	 * @param Printout Gibt an ob bei einer Suche nach dem Text, der Text auf der konsole ausgegeben werden soll @see get/setPrintout
	 */
	public SearchReturnSave(ITextSave next, boolean Printout) {
		super(next);
		this.Printout = Printout;
        if(next != null)
            throw new IllegalArgumentException("next must be null at SearchReturnSave");
	}

	@Override
	public ITextWithLanguage GetLocal(final String search, final Locale Language) {
		if(Printout)
			System.out.println("Text nicht gefunden: \"" + search + "\" in der Sprache: " + Language);
		return new TextWithLanguage(search, search, Language);
	}
}
