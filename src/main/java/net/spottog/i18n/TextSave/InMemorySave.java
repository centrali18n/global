package net.spottog.i18n.TextSave;

import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import net.spottog.i18n.global.ITextSave;
import net.spottog.i18n.global.ITextWithLanguage;
/**
 * Ein TextSave welcher die Texte im Ram Hällt.
 * Durch die Verkettung ist er dazu gedacht Langsame zugriffe z.B. übers Netzwert zu reduzieren.
 * Jede Anfrage wird einmal durchgereicht und Anschließend aus dem Arbeitsspeicher beantwortet.
 */
public class InMemorySave extends ITextSave {

	private ConcurrentHashMap<Locale, ConcurrentHashMap<String, ITextWithLanguage>> localeSave = new ConcurrentHashMap<Locale, ConcurrentHashMap<String, ITextWithLanguage>>();
	/**
	 * Initialisiert eine Instanz des InMemory Text saves
	 * @param next nächster Text Save in der Verkettung.
	 */
	public InMemorySave(ITextSave next) {
		super(next);
	}

	@Override
	public ITextWithLanguage GetLocal(String search, Locale Language) {
		ConcurrentHashMap<String, ITextWithLanguage> lang = localeSave.getOrDefault(Language, null);
		if (lang == null)
			return null;
		return lang.getOrDefault(search, null);
	}

	@Override
	public void Add(ITextWithLanguage internationaText) {
		super.Add(internationaText);
		ConcurrentHashMap<String, ITextWithLanguage> language = localeSave.computeIfAbsent(
				internationaText.getLanguage()==null ? Locale.forLanguageTag("") : internationaText.getLanguage(), new Function<Locale, ConcurrentHashMap<String, ITextWithLanguage>>() {
					public ConcurrentHashMap<String, ITextWithLanguage> apply(Locale key) {
						return new ConcurrentHashMap<String, ITextWithLanguage>();
					}
				});
		language.put(internationaText.getSearchString(), internationaText);
	}
}
