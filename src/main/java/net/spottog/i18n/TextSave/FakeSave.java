package net.spottog.i18n.TextSave;

import java.util.Locale;

import net.spottog.i18n.global.ITextSave;
import net.spottog.i18n.global.ITextWithLanguage;
import net.spottog.i18n.global.TextWithLanguage;
/**
 * der Fake Save, ist wie der Name vermuten lässt, eine für debugging zwecke entwickelte Klasse welche nicht Speichert.
 * Es wird der Suchstring zusammen mit einer Länder kennung zurückgegeben.
 * dies sieht so aus: [de]Hallo 
 *
 */
public class FakeSave extends ITextSave {
	public boolean Printout;
	/**
	 * Erstellt einen Fake Save
	 * @param next muss null sein.
	 */
	public FakeSave(ITextSave next) {
		this(next, false);
	}
	/**
	 * Erstellt einen Fake Save
	 * @param next muss null sein.
	 * @param Printout mithilfe vom Printout, kann angeben werden, ob eine Meldung ausgegeben werden soll.
	 */
	public FakeSave(ITextSave next, boolean Printout) {
		super(next);
		this.Printout = Printout;
        if(next != null)
            throw new IllegalArgumentException("next must be null at FakeSave");
	}

	@Override
	public ITextWithLanguage GetLocal(String search, Locale Language) {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(Language.getLanguage());
		sb.append("] ");
		sb.append(search);
		String text = sb.toString();
		if(Printout)
			System.out.println("Textnot Found: " + text);
		return new TextWithLanguage(search, text, Language);
	}
}
