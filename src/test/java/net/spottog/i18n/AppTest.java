package net.spottog.i18n;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.Locale;


import org.junit.jupiter.api.Test;
import net.spottog.i18n.TextSave.*;
import net.spottog.i18n.global.I18n;
import net.spottog.i18n.global.TextWithLanguage;

/**
 * Test FÜr die I18n Bibliotek
 */
public class AppTest 
{
    /**
     * test case
     *
     */
	@Test
    public void OutputTest()
    {
        I18n.Save = new InMemorySave(new FakeSave(null, true));
        I18n i = new I18n("Hallo, ", new I18n("Wie geht es dir"));
        assertNotEquals("Hallo, Wie geht es dir", i.toString());
        i = new I18n("Hallo, ", new I18n("Wie geht es dir", Locale.GERMANY), Locale.GERMANY);
        assertEquals("Hallo, Wie geht es dir", i.toString());
        System.out.println(i.toString());
        I18n.Save.Add(new TextWithLanguage("Hallo, ", "Hallo, ", I18n.DefaultCultureInfo()));
        I18n.Save.Add(new TextWithLanguage("Wie geht es dir", "Wie geht es dir", I18n.DefaultCultureInfo()));
        I18n.Save.Add(new TextWithLanguage("Hallo, ", "Hello, ", Locale.ENGLISH));
        I18n.Save.Add(new TextWithLanguage("Wie geht es dir", "How are you", Locale.ENGLISH));
        assertEquals("Hallo, Wie geht es dir", i.toString());
        System.out.println(i.toString());
        assertEquals("Hello, How are you", i.toString(Locale.ENGLISH));
        System.out.println(i.toString(Locale.ENGLISH));
    }
}
